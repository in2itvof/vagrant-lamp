class system::install {

  include system::params
  include system::install::packages

  service {'memcached':
    name => 'memcached',
    ensure => running,
    enable => true,
    require => Package['memcached']
  }

}
