class system::install::packages {
  
  include system::params

  package { $system::params::packages:
    ensure => latest,
    provider => apt,
    require => Exec['apt-update']
  }
  
  if $require {
    Package[$system::params::packages] {
      require +> $require
    }
  }

}
