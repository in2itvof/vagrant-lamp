class system::params {

  $packages = [
    'curl',
    'lynx',
    'wget',
    'memcached',
    'autoconf',
    'make',
    'gcc',
    'subversion',
    'git',
    'vim'
  ]

}
