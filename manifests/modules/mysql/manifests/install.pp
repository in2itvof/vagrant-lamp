class mysql::install {

  include mysql::params
  include mysql::install::packages

  service {'mysql':
    ensure => running,
    enable => true,
    require => Package['mysql-server']
  }

  exec { "update-root":
     command => "mysqladmin -u root password root",
     onlyif => "mysql -uroot > /dev/null 2>&1",
     notify => Service["mysql"],
     require => Package["mysql-server"]
  }

  file { "/root/.my.cnf":
    ensure => file,
    content => "[client]\nuser=root\npassword=root\n",
    require => Exec["update-root"],
    notify => Service["mysql"],
    before => Exec['phpmyadmin-configuration-options']
  }
}