class mysql::install::packages {

  include mysql::params

  package { $mysql::params::packages:
    ensure => latest,
    provider => apt,
    notify => Service['mysql'],
    require => Exec['apt-update']
  }

  if $require {
    Package[$mysql::params::packages] {
      require +> $require
    }
  }
}