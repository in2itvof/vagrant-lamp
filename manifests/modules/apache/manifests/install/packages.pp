class apache::install::packages {
  
  include apache::params

  package { $apache::params::packages:
    ensure => latest,
    provider => apt,
    notify => Service['apache2'],
    require => Exec['apt-update']
  }
  
  if $require {
    Package[$apache::params::packages] {
      require +> $require
    }
  }

  service { 'apache2':
    name => 'apache2',
    ensure => 'running',
    enable => true,
  }
}
