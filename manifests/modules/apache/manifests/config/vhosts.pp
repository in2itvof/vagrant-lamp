class apache::config::vhosts {

  include apache::params

  apache::config::vhosts-configurator{ $apache::params::vhosts: }

}

define apache::config::vhosts-configurator () {

  # Global settings
  $config = "${name}.conf"
  $path = "/etc/apache2/sites-available/${config}"
  $activate = "a2ensite ${config}"
  $port = 80
  $serverName = "${name}.local"
  $documentRoot = "/var/www/${serverName}"

  file { $documentRoot:
    ensure => link,
    target => '/vagrant',
    require => Package['apache2'],
    notify => Service['apache2']
  }

  file { $config:
    path => $path,
    ensure => file,
    content => template('apache/vhost.conf.erb'),
    require => Package['apache2'],
    notify => Exec[$activate]
  }

  exec { $activate:
    command => $activate,
    unless => "find /etc/apache2/sites-enabled | grep -c ${config}",
    require => File[$config],
    notify => Service['apache2']
  }

}
