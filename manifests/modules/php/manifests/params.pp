class php::params {

  $packages = [
    'php5',
    'php5-cli',
    'php5-common',
    'php5-curl',
    'php5-dev',
    'php5-mcrypt',
    'php5-intl',
    'php5-memcache',
    'php5-mysql',
    'php5-sqlite',
    'php5-xdebug',
    'libapache2-mod-php5',
    'zend-framework-bin'
  ]

  $tools = [
    'phpunit',
    'zf'
  ]

  $extensions = [
    'php5-redis'
  ]
}
