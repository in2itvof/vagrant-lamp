class php::install::packages {
  
  include php::params

  package { $php::params::packages:
    ensure => latest,
    provider => apt,
    notify => Exec['enable-php5'],
    require => Exec['apt-update']
  }
  
  if $require {
    Package[$php::params::packages] {
      require +> $require
    }
  }

}
