class php::install {

  include php::params
  include php::install::packages
  include php::install::tools

  exec {'enable-php5':
    command => 'a2enmod php5',
    unless => 'find /etc/apache2/mods-enabled | grep -c php',
    require => Package['libapache2-mod-php5'],
    notify => Service['apache2']
  }

  file {'/etc/php5/conf.d/expose_php.ini':
    ensure => file,
    content => 'expose_php = 0',
    require => Package['php5']
  }

  $xdebug_settings = "xdebug.default_enable = 1\nxdebug.force_display_errors = 1\nxdebug.cli_color = 1\nxdebug.remote_connect_back = 1\nxdebug.remote_enable = 1\nxdebug.scream = 1"
  exec {'xdebug-boost':
    command => "echo \"$xdebug_settings\" >> /etc/php5/conf.d/xdebug.ini",
    unless => "grep -c scream /etc/php5/conf.d/xdebug.ini",
    require => Package['php5-xdebug'],
    notify => Service['apache2']
  }

  $redis_settings = "[redis]\nextension=redis.so\n"
  file { '/etc/php5/conf.d/redis.ini':
    ensure => file,
    content => $redis_settings,
    require => Exec['install-php5-redis.sh'],
    notify => Service['apache2']
  }
}
