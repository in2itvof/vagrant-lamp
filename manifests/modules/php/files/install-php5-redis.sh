#!/bin/sh

REDIS_VERSION=2.2.7
PACKAGE_URL=http://pecl.php.net/get/redis-$REDIS_VERSION.tgz

cd /tmp
wget $PACKAGE_URL
tar -xzf redis-$REDIS_VERSION.tgz

cd redis-$REDIS_VERSION
phpize
./configure && make && make install

echo "Cleaning up"
rm -rf /tmp/redis-$REDIS_VERSION.tgz
rm -rf /tmp/redis-$REDIS_VERSION

exit 0