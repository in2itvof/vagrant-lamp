#!/bin/sh

VERSION="3.4.15"
PHPUNIT="phpunit-$VERSION"
URL="https://github.com/sebastianbergmann/phpunit/archive/$VERSION.tar.gz"
INCLUDE_PATH=/usr/share/php

cd /tmp
echo "Fetching $PHPUNIT_VER.tar.gz"
wget -q -O /tmp/$PHPUNIT_VER.tar.gz $URL

echo "Unpacking $PHPUNIT"
tar -xzf $PHPUNIT_VER.tar.gz

echo "Installing $PHPUNIT"
cd $PHPUNIT
mkdir -p $INCLUDE_PATH
cp -rp PHPUnit $INCLUDE_PATH/PHPUnit
cp phpunit.php /usr/local/bin/phpunit
cp dbunit.php /usr/local/bin/dbunit

echo "Cleaning up"
rm -rf /tmp/$PHPUNIT.tgz
rm -rf /tmp/$PHPUNIT