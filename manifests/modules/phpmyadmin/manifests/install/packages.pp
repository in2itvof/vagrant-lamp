class phpmyadmin::install::packages {

  include phpmyadmin::params

  package { $phpmyadmin::params::packages:
    ensure => latest,
    provider => apt,
    notify => Service['mysql'],
    require => Exec['apt-update']
  }

  if $require {
    Package[$phpmyadmin::params::packages] {
      require +> $require
    }
  }
}