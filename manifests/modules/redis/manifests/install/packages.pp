class redis::install::packages {
  include redis::params

  package { $redis::params::packages:
    ensure => latest,
    provider => apt,
    require => Exec['apt-update']
  }

  if $require {
    Package[$redis::params::packages] {
      require +> $require
    }
  }
}